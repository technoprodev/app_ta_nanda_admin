<?php
$params['user.passwordResetTokenExpire'] = 3600;
$params['slider'] = false;

$config = [
    'id' => 'app_ta_nanda_admin',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'app_ta_nanda_admin\controllers',
    'bootstrap' => ['log'],
    'modules' => [],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-app_ta_nanda_admin',
        ],
        'user' => [
            'identityClass' => 'app_ta_nanda_admin\models\UserIdentity',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-app_ta_nanda_admin', 'httpOnly' => true],
        ],
        'session' => [
            'name' => 'session-app_ta_nanda_admin',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
    ],
    'params' => $params,
];

return $config;