<?php
namespace app_ta_nanda_admin\console\controllers;

use yii\console\Controller;

class DevController extends Controller
{
    public $option1 = 'option1';
    public $option2 = 'option2';

    public function options($actionID) {
        return array_merge(parent::options($actionID), ['option1', 'option2']);
    }
    
    public function optionAliases()
    {
        return ['1' => 'option1', '2' => 'option2'];
    }

    public function actionTest($param1 = 'param1', $param2 = 'param2')
    {
        echo "$param1 + $param2\n";
        echo "$this->option1 + $this->option2\n";
        echo "\n";
    }
    
    public function actionIndex()
    {
        \Yii::$app->runAction('dev/test'); // yii dev/test
        \Yii::$app->runAction('dev/test', array('param11', 'param22')); // yii dev/test param11 param22
        \Yii::$app->runAction('dev/test', array('option1'=>'option11', 'option2'=>'option22')); // yii dev/test --option1=option11 --option2=option22
        \Yii::$app->runAction('dev/test', array('1'=>'option11', '2'=>'option22')); // yii dev/test --1=option11 --2=option22
        \Yii::$app->runAction('dev/test', array('param11', 'param22', '1'=>'option11', '2'=>'option22')); // yii dev/test param11 param22 --1=option11 --2=option22
    }
}