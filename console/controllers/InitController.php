<?php
namespace app_ta_nanda_admin\console\controllers;

use yii\console\Controller;
use technosmart\models\Permission;

class InitController extends Controller
{
    public $dir = [
        'app_ta_nanda_admin\controllers' => '@app_ta_nanda_admin/controllers',
    ];

    public function actionIndex()
    {
        \Yii::$app->runAction('cache/flush-all');

        \Yii::$app->cache->cachePath = \Yii::getAlias('@app_ta_nanda_admin/runtime/cache');
        \Yii::$app->runAction('cache/flush-all');

        if (Permission::initAllController($this->dir))
            return 0;
        else
            return 1;
    }
}