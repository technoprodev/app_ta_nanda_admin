<?php
namespace app_ta_nanda_admin\controllers;

use Yii;
use app_ta_nanda_admin\models\Dev;
use app_ta_nanda_admin\models\DevExtend;
use app_ta_nanda_admin\models\DevChild;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;

/**
 * DevController implements an highly advanced CRUD actions for Dev model.
 */
class DevController extends \technosmart\yii\web\Controller
{
    public function actionDatatables()
    {
        $db = Dev::getDb();
        $post = Yii::$app->request->post();

        // serve data for datatables
        if (isset($post['draw'])) {
            $query = new \yii\db\Query();
            $query
                ->select('count(*)')
                ->from('dev d')
                ->join('LEFT JOIN', 'dev_category_option dco', 'dco.id = d.link')
                ->join('LEFT JOIN', 'dev_extend de', 'de.id_dev = d.id');
            $countWhere = count($query->where);

            $total = $query->scalar($db);
            $return['recordsTotal'] = $total;
            $return['recordsFiltered'] = $total;

            $allWhere = ['or'];
            $allSearch = $post['search']['value'];
            foreach ($post['columns'] as $key => $value) {
                if ($value['searchable'] == 'true') {
                    $column = $value['data'];
                    if (is_array($column)) {
                        if ( isset($column['filter']) )
                            $column = $column['filter'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['search']['regex'] == 'false') {
                        $query->andFilterWhere(['like', $column, $value['search']['value']]);
                    } else if ($value['search']['regex'] == 'true') {
                        $query->andFilterWhere(['regexp', $column, $value['search']['value']]);
                    }

                    if ($allSearch) {
                        if ($post['search']['regex'] == 'false') {
                            $allWhere[] = ['like', $column, $allSearch];
                        } else if ($post['search']['regex'] == 'true') {
                            $allWhere[] = ['regexp', $column, $allSearch];
                        }
                    }
                }
            }
            if (count($allWhere) > 1)
                $query->andFilterWhere($allWhere);
            if (count($query->where) > $countWhere)
                $return['recordsFiltered'] = $query->scalar($db);

            $query->select([
                'd.id',
                'd.id_combination',
                'd.id_user_defined',
                'dco.name AS link',
                'de.extend AS extend',
            ]);

            $order = [];
            if (isset($post['order'])) {
                foreach ($post['order'] as $key => $value) {
                    $column = $post['columns'][$value['column']]['data'];
                    if ($post['columns'][$value['column']]['orderable'] == 'false') {
                        continue;
                    }
                    if (is_array($column)) {
                        if ( isset($column['sort']) )
                            $column = $column['sort'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['dir'] == 'asc')
                        $order[$column] = SORT_ASC;
                    else if ($value['dir'] == 'desc')
                        $order[$column] = SORT_DESC;
                }
            }
            count($order) ? $query->orderBy($order) : 0;

            if (isset($post['length']))
                $query->limit(intval($post['length']));

            if (isset($post['start']))
                $query->offset(intval($post['start']));

            $return['draw'] = intval($post['draw']);
            $return['data'] = $query->all($db);
            return $this->json($return);
        }
    }

    public function actionIndex($id = null)
    {
        if (!$id) {
            return $this->render('list', [
                'title' => 'List of Datas',
            ]);
        }

        $model['dev'] = $this->findModel($id);
        return $this->render('one', [
            'model' => $model,
            'title' => 'Detail of ' . $model['dev']->id_combination,
        ]);
    }

    public function actionCreate()
    {
        $render = false;

        $model['dev'] = isset($id) ? $this->findModel($id) : new Dev();
        $model['dev_extend'] = isset($model['dev']->devExtend) ? $model['dev']->devExtend : new DevExtend();
        if (count($model['dev']->devChildren) > 0) {
            foreach ($model['dev']->devChildren as $key => $value) {
                $model['dev_child'][$value->id] = $value;
            }
        } else {
            $model['dev_child']['new'][] = new DevChild();
        }

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            if (!isset($post['Dev']['virtual_category'])) {
                $post['Dev']['virtual_category'] = [];
            }

            $model['dev']->load($post);
            $model['dev_extend']->load($post);
            if (isset($post['DevChild'])) {
                foreach ($post['DevChild'] as $key => $value) {
                    if ($key != 'new') {
                        $model['dev_child'][$key] = $this->findModelChild($key);
                        $model['dev_child'][$key]->setAttributes($value);
                    } else {
                        $model['dev_child']['new'] = null;
                        foreach ($value as $key => $val) {
                            $devChild = new DevChild();
                            $devChild->setAttributes($val);
                            $model['dev_child']['new'][] = $devChild;
                        }
                    }
                }
            }

            if (Yii::$app->request->isAjax && isset($post['ajax'])) {
                $result = array_merge(
                    ActiveForm::validate($model['dev']),
                    ActiveForm::validate($model['dev_extend'])
                );
                /*foreach ($model['dev_child'] as $key => $value) {
                    if ($key != 'new') {
                        $result[] = ActiveForm::validate($model['dev_child'][$key]);
                    } else {
                        foreach ($value as $key => $val) {
                            $result[] = ActiveForm::validate($model['dev_child']['new'][$key]);
                        }
                    }
                }*/
                return $this->json($result);
            }

            $transaction['dev'] = Dev::getDb()->beginTransaction();

            try {
                if ($model['dev']->isNewRecord) {}
                if (!$model['dev']->save()) {
                    throw new \yii\base\UserException('Data tidak berhasil disimpan. Harap lakukan pengisian data kembali.');
                }
                
                $model['dev_extend']->id_dev = $model['dev']->id;
                if (!$model['dev_extend']->save()) {
                    throw new \yii\base\UserException('Data tidak berhasil disimpan. Harap lakukan pengisian data kembali.');
                }

                if (isset($model['dev_child'])) {
                    foreach ($model['dev_child'] as $key => $value) {
                        if ($key < 0) {
                            if (!$model['dev_child'][$key]->delete()) {
                                throw new \yii\base\UserException('Data tidak berhasil dihapus.');
                            }
                        } else if ($key != 'new') {
                            $model['dev_child'][$key]->id_dev = $model['dev']->id;
                            if (!$model['dev_child'][$key]->save()) {
                                throw new \yii\base\UserException('Data tidak berhasil disimpan. Harap lakukan pengisian data kembali.');
                            }
                        } else {
                            foreach ($value as $key => $val) {
                                $model['dev_child']['new'][$key]->id_dev = $model['dev']->id;
                                if (!$model['dev_child']['new'][$key]->save()) {
                                    throw new \yii\base\UserException('Data tidak berhasil disimpan. Harap lakukan pengisian data kembali.');
                                }
                            }
                        }
                    }
                }
                
                $transaction['dev']->commit();
            } catch (\Exception $e) {
                $render = true;
                $transaction['dev']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['dev']->rollBack();
            }
        } else {
            if ($model['dev']->isNewRecord) {
                $model['dev']->id_combination = $this->sequence('dev-id_combination');
            }
            $render = true;
        }

        if ($render)
            return $this->render('form', [
                'model' => $model,
                'title' => 'Create New Data',
            ]);
        else
            return $this->redirect(['index', 'id' => $model['dev']->id]);
    }

    public function actionUpdate($id)
    {
        $render = false;

        $model['dev'] = isset($id) ? $this->findModel($id) : new Dev();
        $model['dev_extend'] = isset($model['dev']->devExtend) ? $model['dev']->devExtend : new DevExtend();
        if (count($model['dev']->devChildren) > 0) {
            foreach ($model['dev']->devChildren as $key => $value) {
                $model['dev_child'][$value->id] = $value;
            }
        } else {
            $model['dev_child']['new'][] = new DevChild();
        }

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            if (!isset($post['Dev']['virtual_category'])) {
                $post['Dev']['virtual_category'] = [];
            }

            $model['dev']->load($post);
            $model['dev_extend']->load($post);
            if (isset($post['DevChild'])) {
                foreach ($post['DevChild'] as $key => $value) {
                    if ($key != 'new') {
                        $model['dev_child'][$key] = $this->findModelChild($key);
                        $model['dev_child'][$key]->setAttributes($value);
                    } else {
                        $model['dev_child']['new'] = null;
                        foreach ($value as $key => $val) {
                            $devChild = new DevChild();
                            $devChild->setAttributes($val);
                            $model['dev_child']['new'][] = $devChild;
                        }
                    }
                }
            }

            if (Yii::$app->request->isAjax && isset($post['ajax'])) {
                $result = array_merge(
                    ActiveForm::validate($model['dev']),
                    ActiveForm::validate($model['dev_extend'])
                );
                /*foreach ($model['dev_child'] as $key => $value) {
                    if ($key != 'new') {
                        $result[] = ActiveForm::validate($model['dev_child'][$key]);
                    } else {
                        foreach ($value as $key => $val) {
                            $result[] = ActiveForm::validate($model['dev_child']['new'][$key]);
                        }
                    }
                }*/
                return $this->json($result);
            }

            $transaction['dev'] = Dev::getDb()->beginTransaction();

            try {
                if ($model['dev']->isNewRecord) {}
                if (!$model['dev']->save()) {
                    throw new \yii\base\UserException('Data tidak berhasil disimpan. Harap lakukan pengisian data kembali.');
                }
                
                $model['dev_extend']->id_dev = $model['dev']->id;
                if (!$model['dev_extend']->save()) {
                    throw new \yii\base\UserException('Data tidak berhasil disimpan. Harap lakukan pengisian data kembali.');
                }

                if (isset($model['dev_child'])) {
                    foreach ($model['dev_child'] as $key => $value) {
                        if ($key < 0) {
                            if (!$model['dev_child'][$key]->delete()) {
                                throw new \yii\base\UserException('Data tidak berhasil dihapus.');
                            }
                        } else if ($key != 'new') {
                            $model['dev_child'][$key]->id_dev = $model['dev']->id;
                            if (!$model['dev_child'][$key]->save()) {
                                throw new \yii\base\UserException('Data tidak berhasil disimpan. Harap lakukan pengisian data kembali.');
                            }
                        } else {
                            foreach ($value as $key => $val) {
                                $model['dev_child']['new'][$key]->id_dev = $model['dev']->id;
                                if (!$model['dev_child']['new'][$key]->save()) {
                                    throw new \yii\base\UserException('Data tidak berhasil disimpan. Harap lakukan pengisian data kembali.');
                                }
                            }
                        }
                    }
                }
                
                $transaction['dev']->commit();
            } catch (\Exception $e) {
                $render = true;
                $transaction['dev']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['dev']->rollBack();
            }
        } else {
            if ($model['dev']->isNewRecord) {
                $model['dev']->id_combination = $this->sequence('dev-id_combination');
            }
            $render = true;
        }

        if ($render)
            return $this->render('form', [
                'model' => $model,
                'title' => 'Update Data ' . $model['dev']->id_combination,
            ]);
        else
            return $this->redirect(['index', 'id' => $model['dev']->id]);
    }

    public function actionTest()
    {
        \Yii::$app->dba->createCommand()->delete('dev_category', 'id_dev = 24')->execute();
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = Dev::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelChild($id)
    {
        if (($model = DevChild::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    ////

    protected function arrayDiff($current = [], $new = [])
    {
        /*$update_diff = [];
        $insert_diff = array_diff_ukey($new, $current, function($x, $y) use ($current, $new, &$update_diff) {
            if ($x == $y) {
                if ($current[$x] != $new[$x]) {
                    $update_diff[] = $new[$x];
                }

                return 0;
            }
            else return $x > $y ? 1 : -1;
        });*/
        $update_diff = count($new) > 0 ? array_intersect_key($new, $current) : [];
        $insert_diff = count($new) > 0 ? array_diff_key($new, $current) : [];
        $delete_diff = count($current) > 0 ? array_diff_key($current, $new) : [];

        return [$insert_diff, $update_diff, $delete_diff];
    }

    public function actionArrayDiff()
    {
        $current = [
            '0a' => 'nol',
            '1a' => 'satu',
            '2a' => 'dua',
            '3a' => 'tiga',
        ];

        $new = [
            '1a' => 'satu',
            '2a' => 'two',
            '3a' => 'three',
            '4a' => 'four',
        ];
        
        // Yii::$app->debug->debugx($this->arrayDiff($current, $new));

        $query = new \yii\db\Query();
        $query
            ->select('*')
            ->from('dev');
        $query->orWhere(['id'=>'12', 'id_user_defined'=>'syiwa']);
        $query->andWhere('1=1');
        $a = '';
        $query->andFilterWhere(['or', ['like', 'id', 12], ['like', 'id', $a]]);

        $result = $query->createCommand()->sql;
        Yii::$app->debug->debugx($result);
        Yii::$app->debug->debugx($query->all());
    }
}