<?php
namespace app_ta_nanda_admin\controllers;

use Yii;
use technosmart\controllers\SiteController as SiteControl;
use app_ta_nanda_admin\models\UserIdentity;

class SiteController extends SiteControl
{
    public function behaviors()
    {
        return [
            'access' => $this->access([
                [['index', 'register', 'login', 'error'], true, ['?', '@'], ['GET', 'POST']],
                [['logout'], true, ['@'], ['POST']],
            ]),
        ];
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        if ( !Yii::$app->request->isPost || !($model['userIdentity'] = UserIdentity::findByLogin(Yii::$app->request->post()['UserIdentity']['login'])) )
            $model['userIdentity'] = new UserIdentity();
        $model['userIdentity']->scenario = 'login';

        if ($model['userIdentity']->load(Yii::$app->request->post()) && $model['userIdentity']->login()) {
            return $this->goBack();
        } else {
            $this->layout = 'empty';
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }
}