<?php
namespace app_ta_nanda_admin\controllers;

use Yii;
use app_ta_nanda_admin\models\Transaction;
use app_ta_nanda_admin\models\UserIdentity;
use technosmart\yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;

/**
 * TransactionController implements highly advanced CRUD actions for Transaction model.
 */
class TransactionController extends Controller
{
    /*public static $permissions = [
        ['view', 'View Transaction'], ['create', 'Create Transaction'], ['update', 'Update Transaction'], ['delete', 'Delete Transaction'],
    ];

    public function behaviors()
    {
        return [
            'access' => $this->access([
                [['index'], 'view'],
                [['index', 'create'], 'create'],
                [['index', 'update'], 'update'],
                [['index', 'delete'], 'delete', null, ['POST']],
            ]),
        ];
    }*/

    public function actionDatatables()
    {
        $db = Transaction::getDb();
        $post = Yii::$app->request->post();

        // serve data for datatables
        if (isset($post['draw'])) {
            $query = new \yii\db\Query();
            $query
                ->select('count(*)')
                ->from('transaction')
                ->where(['not', ['transaction_date' => null]]);
            $countWhere = count($query->where);

            $total = $query->scalar($db);
            $return['recordsTotal'] = $total;
            $return['recordsFiltered'] = $total;

            $allWhere = ['or'];
            $allSearch = $post['search']['value'];
            foreach ($post['columns'] as $key => $value) {
                if ($value['searchable'] == 'true') {
                    $column = $value['data'];
                    if (is_array($column)) {
                        if ( isset($column['filter']) )
                            $column = $column['filter'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['search']['regex'] == 'false') {
                        $query->andFilterWhere(['like', $column, $value['search']['value']]);
                    } else if ($value['search']['regex'] == 'true') {
                        $query->andFilterWhere(['regexp', $column, $value['search']['value']]);
                    }

                    if ($allSearch) {
                        if ($post['search']['regex'] == 'false') {
                            $allWhere[] = ['like', $column, $allSearch];
                        } else if ($post['search']['regex'] == 'true') {
                            $allWhere[] = ['regexp', $column, $allSearch];
                        }
                    }
                }
            }
            if (count($allWhere) > 1)
                $query->andFilterWhere($allWhere);
            if (count($query->where) > $countWhere)
                $return['recordsFiltered'] = $query->scalar($db);

            $query->select([
                'id',
                'id_customer',
                'name',
                'phone',
                'email',
                'service_type',
                'service_name',
                'harga',
                'booking_date',
                'appoinment_date',
                'transaction_date',
                'note',
            ]);

            $order = [];
            if (isset($post['order'])) {
                foreach ($post['order'] as $key => $value) {
                    $column = $post['columns'][$value['column']]['data'];
                    if ($post['columns'][$value['column']]['orderable'] == 'false') {
                        continue;
                    }
                    if (is_array($column)) {
                        if ( isset($column['sort']) )
                            $column = $column['sort'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['dir'] == 'asc')
                        $order[$column] = SORT_ASC;
                    else if ($value['dir'] == 'desc')
                        $order[$column] = SORT_DESC;
                }
            }
            count($order) ? $query->orderBy($order) : 0;

            if (isset($post['length']))
                $query->limit(intval($post['length']));

            if (isset($post['start']))
                $query->offset(intval($post['start']));

            $return['draw'] = intval($post['draw']);
            $return['data'] = $query->all($db);
            return $this->json($return);
        }
    }

    /**
     * If param(s) is null, display all datas from models.
     * If all param(s) is not null, display a data from model.
     * @param integer $id
     * @return mixed
     */
    public function actionIndex($id = null)
    {
        // view all data
        if (!$id) {
            return $this->render('list', [
                'title' => 'List of Transactions',
            ]);
        }
        
        // view single data
        $model['transaction'] = $this->findModel($id);
        return $this->render('one', [
            'model' => $model,
            'title' => 'Bukti Transaksi ' . $model['transaction']->id,
        ]);
    }

    public function actionDatatablesBooking()
    {
        $db = Transaction::getDb();
        $post = Yii::$app->request->post();

        // serve data for datatables
        if (isset($post['draw'])) {
            $query = new \yii\db\Query();
            $query
                ->select('count(*)')
                ->from('transaction')
                ->where(['transaction_date' => null]);
            $countWhere = count($query->where);

            $total = $query->scalar($db);
            $return['recordsTotal'] = $total;
            $return['recordsFiltered'] = $total;

            $allWhere = ['or'];
            $allSearch = $post['search']['value'];
            foreach ($post['columns'] as $key => $value) {
                if ($value['searchable'] == 'true') {
                    $column = $value['data'];
                    if (is_array($column)) {
                        if ( isset($column['filter']) )
                            $column = $column['filter'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['search']['regex'] == 'false') {
                        $query->andFilterWhere(['like', $column, $value['search']['value']]);
                    } else if ($value['search']['regex'] == 'true') {
                        $query->andFilterWhere(['regexp', $column, $value['search']['value']]);
                    }

                    if ($allSearch) {
                        if ($post['search']['regex'] == 'false') {
                            $allWhere[] = ['like', $column, $allSearch];
                        } else if ($post['search']['regex'] == 'true') {
                            $allWhere[] = ['regexp', $column, $allSearch];
                        }
                    }
                }
            }
            if (count($allWhere) > 1)
                $query->andFilterWhere($allWhere);
            if (count($query->where) > $countWhere)
                $return['recordsFiltered'] = $query->scalar($db);

            $query->select([
                'id',
                'id_customer',
                'name',
                'phone',
                'email',
                'service_type',
                'service_name',
                'harga',
                'booking_date',
                'appoinment_date',
                'transaction_date',
                'note',
            ]);

            $order = [];
            if (isset($post['order'])) {
                foreach ($post['order'] as $key => $value) {
                    $column = $post['columns'][$value['column']]['data'];
                    if ($post['columns'][$value['column']]['orderable'] == 'false') {
                        continue;
                    }
                    if (is_array($column)) {
                        if ( isset($column['sort']) )
                            $column = $column['sort'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['dir'] == 'asc')
                        $order[$column] = SORT_ASC;
                    else if ($value['dir'] == 'desc')
                        $order[$column] = SORT_DESC;
                }
            }
            count($order) ? $query->orderBy($order) : 0;

            if (isset($post['length']))
                $query->limit(intval($post['length']));

            if (isset($post['start']))
                $query->offset(intval($post['start']));

            $return['draw'] = intval($post['draw']);
            $return['data'] = $query->all($db);
            return $this->json($return);
        }
    }

    public function actionBooking($id = null)
    {
        // view all data
        if (!$id) {
            return $this->render('list-booking', [
                'title' => 'List of Bookings',
            ]);
        }
        
        // view single data
        $model['transaction'] = $this->findModel($id);
        return $this->render('one', [
            'model' => $model,
            'title' => 'Detail of Booking ' . $model['transaction']->id,
        ]);
    }

    /**
     * Creates new data(s) from model(s).
     * If submission is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionCreate()
    {
        $render = false;

        $model['transaction'] = isset($id) ? $this->findModel($id) : new Transaction();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['transaction']->load($post);

            if (Yii::$app->request->isAjax && isset($post['ajax'])) {
                $result = array_merge(
                    ActiveForm::validate($model['transaction'])
                );
                return $this->json($result);
            }

            $transaction['transaction'] = Transaction::getDb()->beginTransaction();

            try {
                $model['transaction']->transaction_date = new \yii\db\Expression("now()");
                if (!$model['transaction']->save()) {
                    throw new \yii\base\UserException('Data tidak berhasil disimpan. Harap lakukan pengisian data kembali.');
                }
                
                $transaction['transaction']->commit();
            } catch (\Exception $e) {
                $render = true;
                $transaction['transaction']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['transaction']->rollBack();
            }
        } else {
            $render = true;
        }

        if ($render)
            return $this->render('form', [
                'model' => $model,
                'title' => 'Add New Transaction',
            ]);
        else
            return $this->redirect(['index', 'id' => $model['transaction']->id]);
    }

    public function actionCreateBooking($id)
    {
        $render = false;

        $model['transaction'] = isset($id) ? $this->findModel($id) : new Transaction();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['transaction']->load($post);

            if (Yii::$app->request->isAjax && isset($post['ajax'])) {
                $result = array_merge(
                    ActiveForm::validate($model['transaction'])
                );
                return $this->json($result);
            }

            $transaction['transaction'] = Transaction::getDb()->beginTransaction();

            try {
                $model['transaction']->transaction_date = new \yii\db\Expression("now()");
                if (!$model['transaction']->save()) {
                    throw new \yii\base\UserException('Data tidak berhasil disimpan. Harap lakukan pengisian data kembali.');
                }
                
                $transaction['transaction']->commit();
            } catch (\Exception $e) {
                $render = true;
                $transaction['transaction']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['transaction']->rollBack();
            }
        } else {
            $render = true;
        }

        if ($render)
            return $this->render('form-booking', [
                'model' => $model,
                'title' => 'Add New Transaction from Booking',
            ]);
        else
            return $this->redirect(['index', 'id' => $model['transaction']->id]);
    }

    public function actionCreateCustomer($id)
    {
        $render = false;

        $model['transaction'] = new Transaction();
        $model['user'] = UserIdentity::findOne($id);

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['transaction']->load($post);

            if (Yii::$app->request->isAjax && isset($post['ajax'])) {
                $result = array_merge(
                    ActiveForm::validate($model['transaction'])
                );
                return $this->json($result);
            }

            $transaction['transaction'] = Transaction::getDb()->beginTransaction();

            try {
                $model['transaction']->transaction_date = new \yii\db\Expression("now()");
                $model['transaction']->id_customer = $id;
                if (!$model['transaction']->save()) {
                    throw new \yii\base\UserException('Data tidak berhasil disimpan. Harap lakukan pengisian data kembali.');
                }
                
                $transaction['transaction']->commit();
            } catch (\Exception $e) {
                $render = true;
                $transaction['transaction']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['transaction']->rollBack();
            }
        } else {
            $model['transaction']->name = $model['user']->name;
            $model['transaction']->phone = $model['user']->customer->phone;
            $model['transaction']->email = $model['user']->email;
            $render = true;
        }

        if ($render)
            return $this->render('form-customer', [
                'model' => $model,
                'title' => 'Add New Transaction by Registered User',
            ]);
        else
            return $this->redirect(['index', 'id' => $model['transaction']->id]);
    }

    /**
     * Deletes an existing Transaction model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Transaction model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Transaction the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Transaction::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
