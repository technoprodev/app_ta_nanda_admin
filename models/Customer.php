<?php
namespace app_ta_nanda_admin\models;

use Yii;

/**
 * This is the model class for table "customer".
 *
 * @property integer $id
 * @property string $email
 * @property string $phone
 * @property string $pet_1
 * @property string $pet_2
 * @property string $pet_3
 * @property string $alamat
 * @property string $birthplace
 * @property string $birthday
 * @property string $relative_name
 * @property string $relative_email
 *
 * @property Transaction[] $transactions
 */
class Customer extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'customer';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dba');
    }

    public function rules()
    {
        return [
            //id
            [['id'], 'required'],
            [['id'], 'integer'],

            //email
            [['email'], 'string', 'max' => 64],

            //phone
            [['phone'], 'string', 'max' => 16],

            //pet_1
            [['pet_1'], 'string', 'max' => 64],

            //pet_2
            [['pet_2'], 'string', 'max' => 64],

            //pet_3
            [['pet_3'], 'string', 'max' => 64],

            //alamat
            [['alamat'], 'string'],

            //birthplace
            [['birthplace'], 'string', 'max' => 64],

            //birthday
            [['birthday'], 'safe'],

            //relative_name
            [['relative_name'], 'string', 'max' => 64],

            //relative_email
            [['relative_email'], 'string', 'max' => 64],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'phone' => 'Phone',
            'pet_1' => 'Pet 1',
            'pet_2' => 'Pet 2',
            'pet_3' => 'Pet 3',
            'alamat' => 'Alamat',
            'birthplace' => 'Birthplace',
            'birthday' => 'Birthday',
            'relative_name' => 'Relative Name',
            'relative_email' => 'Relative Email',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransactions()
    {
        return $this->hasMany(Transaction::className(), ['id_customer' => 'id']);
    }
}
