<?php
namespace app_ta_nanda_admin\models;

use Yii;

/**
 * This is the model class for table "transaction".
 *
 * @property integer $id
 * @property integer $id_customer
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $service_type
 * @property string $service_name
 * @property integer $harga
 * @property string $booking_date
 * @property string $appoinment_date
 * @property string $transaction_date
 * @property string $note
 *
 * @property Customer $customer
 */
class Transaction extends \technosmart\yii\db\ActiveRecord
{
    public $jenis;

    public static function tableName()
    {
        return 'transaction';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dba');
    }

    public function rules()
    {
        return [
            //id

            //id_customer
            [['id_customer'], 'integer'],
            [['id_customer'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['id_customer' => 'id']],

            //name
            [['name'], 'string', 'max' => 64],

            //phone
            [['phone'], 'string', 'max' => 16],

            //email
            [['email'], 'string', 'max' => 64],

            //service_type
            [['service_type'], 'required'],
            [['service_type'], 'string'],

            //service_name
            [['service_name'], 'required'],
            [['service_name'], 'string', 'max' => 64],

            //harga
            [['harga'], 'integer'],

            //booking_date
            [['booking_date'], 'safe'],

            //appoinment_date
            [['appoinment_date'], 'safe'],

            //transaction_date
            [['transaction_date'], 'safe'],

            //note
            [['note'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_customer' => 'Id Customer',
            'name' => 'Name',
            'phone' => 'Phone',
            'email' => 'Email',
            'service_type' => 'Service Type',
            'service_name' => 'Service Name',
            'harga' => 'Harga',
            'booking_date' => 'Booking Date',
            'appoinment_date' => 'Appoinment Date',
            'transaction_date' => 'Transaction Date',
            'note' => 'Note',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'id_customer']);
    }
}
