<?php
namespace app_ta_nanda_admin\models;

use Yii;

/**
 * This is connector for Yii::$app->user with model User
 *
 * @property integer $id
 * @property string $username
 */
class UserIdentity extends \technosmart\models\User
{
	public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'id']);
    }
}