<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$error = false;
$errorMessage = '';
if ($model['configuration-file']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['configuration-file'], ['class' => '']);
}
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="row margin-left-30 m-margin-left-0">
    <div class="col-xs-12 col-md-6">    
<?php endif; ?>

<?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <?= $form->field($model['configuration-file'], 'target')->begin(); ?>
        <?= Html::activeLabel($model['configuration-file'], 'target', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['configuration-file'], 'target', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['configuration-file'], 'target', ['class' => 'help-block']); ?>
    <?= $form->field($model['configuration-file'], 'target')->end(); ?>

    <?= $form->field($model['configuration-file'], 'alias_upload_root')->begin(); ?>
        <?= Html::activeLabel($model['configuration-file'], 'alias_upload_root', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['configuration-file'], 'alias_upload_root', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['configuration-file'], 'alias_upload_root', ['class' => 'help-block']); ?>
    <?= $form->field($model['configuration-file'], 'alias_upload_root')->end(); ?>

    <?= $form->field($model['configuration-file'], 'alias_download_base_url')->begin(); ?>
        <?= Html::activeLabel($model['configuration-file'], 'alias_download_base_url', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['configuration-file'], 'alias_download_base_url', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['configuration-file'], 'alias_download_base_url', ['class' => 'help-block']); ?>
    <?= $form->field($model['configuration-file'], 'alias_download_base_url')->end(); ?>


    <hr class="margin-y-15">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton($model['configuration-file']->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-default bg-azure rounded-xs border-azure']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default bg-lighter rounded-xs']); ?> 
        <?= Html::a('Back to list', ['index'], ['class' => 'btn btn-default bg-lightest rounded-xs pull-right']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>