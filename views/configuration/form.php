<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$error = false;
$errorMessage = '';
if ($model['configuration']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['configuration'], ['class' => '']);
}
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="row margin-left-30 m-margin-left-0">
    <div class="col-xs-12 col-md-6">    
<?php endif; ?>

<?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <?= $form->field($model['configuration'], 'name')->begin(); ?>
        <?= Html::activeLabel($model['configuration'], 'name', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['configuration'], 'name', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['configuration'], 'name', ['class' => 'help-block']); ?>
    <?= $form->field($model['configuration'], 'name')->end(); ?>

    <?= $form->field($model['configuration'], 'type')->begin(); ?>
        <?= Html::activeLabel($model['configuration'], 'type', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['configuration'], 'type', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['configuration'], 'type', ['class' => 'help-block']); ?>
    <?= $form->field($model['configuration'], 'type')->end(); ?>

    <?= $form->field($model['configuration'], 'value')->begin(); ?>
        <?= Html::activeLabel($model['configuration'], 'value', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['configuration'], 'value', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['configuration'], 'value', ['class' => 'help-block']); ?>
    <?= $form->field($model['configuration'], 'value')->end(); ?>


    <hr class="margin-y-15">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton($model['configuration']->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-default bg-azure rounded-xs border-azure']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default bg-lighter rounded-xs']); ?> 
        <?= Html::a('Back to list', ['index'], ['class' => 'btn btn-default bg-lightest rounded-xs pull-right']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>