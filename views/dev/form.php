<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

technosmart\assets_manager\JqueryInputLimiterAsset::register($this);
technosmart\assets_manager\AutosizeAsset::register($this);

//
$devChildren = null;
if (isset($model['dev_child']))
    foreach ($model['dev_child'] as $key => $value) {
        if ($key == 'new') {
            foreach ($value as $key => $val) {
                $devChildren['new'][$key] = $model['dev_child']['new'][$key]->attributes;
            }
        } else {
            $devChildren[$key] = $model['dev_child'][$key]->attributes;
        }     
    }

$this->registerJs(
    'vm.$data.dev.virtualCategory = ' . json_encode($model['dev']->virtual_category) . ';' .
    'vm.$data.dev.devChildren = Object.assign({}, vm.$data.dev.devChildren, ' . json_encode($devChildren) . ');',
    3
);

//
$error = false;
$errorMessage = '';
$errorVue = false;
if ($model['dev']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['dev'], ['class' => '']);
}

if ($model['dev_extend']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['dev_extend'], ['class' => '']);
}

if (isset($model['dev_child'])) foreach ($model['dev_child'] as $key => $value) {
    if ($key == 'new') {
        foreach ($value as $key => $val) {
            if ($model['dev_child']['new'][$key]->hasErrors()) {
                $error = true;
                $errorMessage .= Html::errorSummary($model['dev_child']['new'][$key], ['class' => '']);
                $errorVue = true; 
            }
        }
    }
    else {
        if ($model['dev_child'][$key]->hasErrors()) {
            $error = true;
            $errorMessage .= Html::errorSummary($model['dev_child'][$key], ['class' => '']);
            $errorVue = true; 
        }
    }
}
if ($errorVue) {
    $this->registerJs(
        '$.each($("#app").data("yiiActiveForm").attributes, function() {
            this.status = 3;
        });
        $("#app").yiiActiveForm("validate");',
        5
    );
}
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="row margin-left-30 m-margin-left-0">
    <div class="col-xs-12 col-md-6">    
<?php endif; ?>

<?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <div class="form-group">
        <label class="control-label"><?= $model['dev']->attributeLabels()['id_combination'] ?></label>
        <?= Html::activeHiddenInput($model['dev'], 'id_combination', ['initial-value ng-model' => 'dev.id_combination']); ?>
        <p class="form-control-static border-bottom"><?= $model['dev']->id_combination ?></p>
    </div>

    <?= $form->field($model['dev'], 'id_combination')->begin(); ?>
        <?= Html::activeLabel($model['dev'], 'id_combination', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['dev'], 'id_combination', ['class' => 'form-control', 'maxlength' => true, 'disabled' => true]); ?>
        <?= Html::error($model['dev'], 'id_combination', ['class' => 'help-block']); ?>
    <?= $form->field($model['dev'], 'id_combination')->end(); ?>

    <?= $form->field($model['dev'], 'id_combination')->begin(); ?>
        <?= Html::activeLabel($model['dev'], 'id_combination', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['dev'], 'id_combination', ['class' => 'form-control', 'maxlength' => true, 'readonly' => true]); ?>
        <?= Html::error($model['dev'], 'id_combination', ['class' => 'help-block']); ?>
    <?= $form->field($model['dev'], 'id_combination')->end(); ?>

    <?= $form->field($model['dev'], 'id_user_defined'/*, ['enableAjaxValidation' => true]*/)->begin(); ?>
        <?= Html::activeLabel($model['dev'], 'id_user_defined', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['dev'], 'id_user_defined', ['class' => 'form-control', 'maxlength' => true, 'initial-value ng-model' => 'dev.id_user_defined']); ?>
        <?= Html::error($model['dev'], 'id_user_defined', ['class' => 'help-block']); ?>
    <?= $form->field($model['dev'], 'id_user_defined')->end(); ?>

    <?= $form->field($model['dev'], 'text')->begin(); ?>
        <?= Html::activeLabel($model['dev'], 'text', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['dev'], 'text', ['class' => 'form-control', 'maxlength' => true, 'v-model' => 'dev.text', 'v-default-value' => "'" . $model['dev']->text . "'"]); ?>
        <?= Html::error($model['dev'], 'text', ['class' => 'help-block']); ?>
    <?= $form->field($model['dev'], 'text')->end(); ?>

    <?= $form->field($model['dev'], 'text')->begin(); ?>
        <?= Html::activeLabel($model['dev'], 'text', ['class' => 'control-label']); ?>
        <?= Html::activePasswordInput($model['dev'], 'text', ['class' => 'form-control', 'maxlength' => true]); ?>
        <?= Html::error($model['dev'], 'text', ['class' => 'help-block']); ?>
    <?= $form->field($model['dev'], 'text')->end(); ?>

    <?= $form->field($model['dev'], 'text')->begin(); ?>
        <?= Html::activeLabel($model['dev'], 'text', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['dev'], 'text', ['class' => 'form-control', 'maxlength' => true, 'list' => 'list-dev-autocomplete']); ?>
        <datalist id="list-dev-autocomplete">
            <?php
            $options = ArrayHelper::map(\app_ta_nanda_admin\models\DevCategoryOption::find()->asArray()->all(), 'id', 'name');
            foreach ($options as $key => $value) { ?>
                <option value="<?= $key ?>"><?= $value ?></option>
            <?php } ?>
        </datalist>
        <?= Html::error($model['dev'], 'text', ['class' => 'help-block']); ?>
    <?= $form->field($model['dev'], 'text')->end(); ?>

    <?= $form->field($model['dev'], 'text')->begin(); ?>
        <?= Html::activeLabel($model['dev'], 'text', ['class' => 'control-label']); ?>
        <?= Html::activeTextArea($model['dev'], 'text', ['class' => 'form-control limited textarea-autosize', 'maxlength' => true]); ?>
        <?= Html::error($model['dev'], 'text', ['class' => 'help-block']); ?>
    <?= $form->field($model['dev'], 'text')->end(); ?>

    <hr>

    <?= $form->field($model['dev'], 'link', ['options' => ['class' => 'form-group radio-elegant']])->begin(); ?>
        <?= Html::activeLabel($model['dev'], 'link', ['class' => 'control-label']); ?>
        <?= Html::activeRadioList($model['dev'], 'link', ArrayHelper::map(\app_ta_nanda_admin\models\DevCategoryOption::find()->indexBy('id')->asArray()->all(), 'id', 'name'), ['class' => 'row row-radio', 'unselect' => null,
            'item' => function($index, $label, $name, $checked, $value){
                $disabled = in_array($value, ['val1', 'val2', '1']) ? 'disabled' : '';
                return "<div class='radio col-xs-3'><label><input type='radio' name='$name' value='$value' $disabled><i></i>$label</label></div>";

                // equal to this
                /*$disabled = in_array($value, ['val1', 'val2', '1']) ? true : false;

                $radio = Html::radio($name, $checked, ['value' => $value, 'disabled' => $disabled]);
                return Html::tag('div', Html::label($radio . '<i></i>' . $label), ['class' => 'radio col-xs-3']);*/
            }]); ?>
        <?= Html::error($model['dev'], 'link', ['class' => 'help-block']); ?>
    <?= $form->field($model['dev'], 'link')->end(); ?>

    <?= $form->field($model['dev'], 'enum')->begin(); ?>
        <?= Html::activeLabel($model['dev'], 'enum', ['class' => 'control-label']); ?>
        <?= Html::activeRadioList($model['dev'], 'enum', $model['dev']->getEnum('enum'), ['unselect' => null,
            'item' => function($index, $label, $name, $checked, $value){
                $disabled = in_array($value, ['val1', 'val2', '1']) ? 'disabled' : '';
                return "<div class='radio'><label><input type='radio' name='$name' value='$value' $disabled><i></i>$label</label></div>";
            }]); ?>
        <?= Html::error($model['dev'], 'enum', ['class' => 'help-block']); ?>
    <?= $form->field($model['dev'], 'enum')->end(); ?>

    <hr>

    <?= $form->field($model['dev'], 'virtual_category', ['options' => ['class' => 'form-group checkbox-elegant']])->begin(); ?>
        <?= Html::activeLabel($model['dev'], 'virtual_category', ['class' => 'control-label']); ?>
        <?= Html::activeCheckboxList($model['dev'], 'virtual_category', ArrayHelper::map(\app_ta_nanda_admin\models\DevCategoryOption::find()->indexBy('id')->asArray()->all(), 'id', 'name'), ['class' => 'row row-checkbox', 'unselect' => null,
            'item' => function($index, $label, $name, $checked, $value){
                $disabled = in_array($value, ['val1', 'val2', '1']) ? 'disabled' : '';
                return "<div class='checkbox col-xs-3'><label><input type='checkbox' name='$name' value='$value' $disabled v-model='dev.virtualCategory'><i></i>$label</label></div>";
            },
        ]); ?>
        <?= Html::error($model['dev'], 'virtual_category', ['class' => 'help-block']); ?>
    <?= $form->field($model['dev'], 'virtual_category')->end(); ?>

    <?= $form->field($model['dev'], 'set', ['options' => ['class' => 'form-group checkbox-fancy']])->begin(); ?>
        <?= Html::activeLabel($model['dev'], 'set', ['class' => 'control-label']); ?>
        <?= Html::activeCheckboxList($model['dev'], 'set', $model['dev']->getSet('set'), ['unselect' => null,
            'item' => function($index, $label, $name, $checked, $value){
                $disabled = in_array($value, ['val1', 'val2', 'technology']) ? 'disabled' : '';
                return "<div class='checkbox'><label><input type='checkbox' name='$name' value='$value' $disabled><i></i>$label</label></div>";
            }
        ]); ?>
        <?= Html::error($model['dev'], 'set', ['class' => 'help-block']); ?>
    <?= $form->field($model['dev'], 'set')->end(); ?>

    <?= $form->field($model['dev'], 'set')->begin(); ?>
        <?= Html::activeLabel($model['dev'], 'set', ['class' => 'control-label']); ?>
        <div class="checkbox">
            <?= Html::activeCheckbox($model['dev'], 'set', ['uncheck' => null]); ?>
        </div>
        <?= Html::error($model['dev'], 'set', ['class' => 'help-block']); ?>
    <?= $form->field($model['dev'], 'set')->end(); ?>

    <hr>

    <?= $form->field($model['dev'], 'link')->begin(); ?>
        <?= Html::activeLabel($model['dev'], 'link', ['class' => 'control-label']); ?>
        <?= Html::activeDropDownList($model['dev'], 'link', ArrayHelper::map(\app_ta_nanda_admin\models\DevCategoryOption::find()->indexBy('id')->asArray()->all(), 'id', 'name', 'parent'), ['prompt' => 'Choose one please', 'class' => 'form-control']); ?>
        <?= Html::error($model['dev'], 'link', ['class' => 'help-block']); ?>
    <?= $form->field($model['dev'], 'link')->end(); ?>

    <!--
    <?= $form->field($model['dev'], 'virtual_category')->begin(); ?>
        <?= Html::activeLabel($model['dev'], 'virtual_category', ['class' => 'control-label']); ?>
        <?= Html::activeListBox($model['dev'], 'virtual_category', ArrayHelper::map(\app_ta_nanda_admin\models\DevCategoryOption::find()->indexBy('id')->asArray()->all(), 'id', 'name'), ['class' => 'form-control', 'multiple' => true, 'unselect' => null, 'v-model' => 'dev.virtualCategory']); ?>
        <?= Html::error($model['dev'], 'virtual_category', ['class' => 'help-block']); ?>
    <?= $form->field($model['dev'], 'virtual_category')->end(); ?>
    -->

    <?= $form->field($model['dev'], 'enum')->begin(); ?>
        <?= Html::activeLabel($model['dev'], 'enum', ['class' => 'control-label']); ?>
        <?= Html::activeDropDownList($model['dev'], 'enum', $model['dev']->getEnum('enum'), ['prompt' => 'Choose one please', 'class' => 'form-control']); ?>
        <?= Html::error($model['dev'], 'enum', ['class' => 'help-block']); ?>
    <?= $form->field($model['dev'], 'enum')->end(); ?>

    <?= $form->field($model['dev'], 'set')->begin(); ?>
        <?= Html::activeLabel($model['dev'], 'set', ['class' => 'control-label']); ?>
        <?= Html::activeListBox($model['dev'], 'set', $model['dev']->getSet('set'), ['class' => 'form-control', 'multiple' => true, 'unselect' => null]); ?>
        <?= Html::error($model['dev'], 'set', ['class' => 'help-block']); ?>
    <?= $form->field($model['dev'], 'set')->end(); ?>

    <hr>

    <?= $form->field($model['dev'], 'file')->begin(); ?>
        <?= Html::activeLabel($model['dev'], 'file', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['dev'], 'file', ['class' => 'form-control']); ?>
        <?= Html::error($model['dev'], 'file', ['class' => 'help-block']); ?>
    <?= $form->field($model['dev'], 'file')->end(); ?>
    
    <hr>

    <?= $form->field($model['dev_extend'], 'extend')->begin(); ?>
        <?= Html::activeLabel($model['dev_extend'], 'extend', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['dev_extend'], 'extend', ['class' => 'form-control', 'maxlength' => true, 'initial-value ng-model' => 'dev.textinput']); ?>
        <?= Html::error($model['dev_extend'], 'extend', ['class' => 'help-block']); ?>
    <?= $form->field($model['dev_extend'], 'extend')->end(); ?>

    <hr>

    <!-- 
    <?php if (isset($model['dev_child'])) foreach ($model['dev_child'] as $key => $value): ?>
        <?php if ($key == 'new') : ?>
            <?php foreach ($value as $key => $val): ?>

                <?= $form->field($model['dev_child']['new'][$key], "[new][$key]child")->begin(); ?>
                    <?= Html::activeLabel($model['dev_child']['new'][$key], "[new][$key]child", ['class' => 'control-label']); ?>
                    <?= Html::activeTextInput($model['dev_child']['new'][$key], "[new][$key]child", ['class' => 'form-control', 'maxlength' => true]); ?>
                    <?= Html::error($model['dev_child']['new'][$key], "[new][$key]child", ['class' => 'help-block']); ?>
                <?= $form->field($model['dev_child']['new'][$key], "[new][$key]child")->end(); ?>

            <?php endforeach; ?>
        <?php else : ?>

            <?= $form->field($model['dev_child'][$key], "[$key]child")->begin(); ?>
                <?= Html::activeLabel($model['dev_child'][$key], "[$key]child", ['class' => 'control-label']); ?>
                <?= Html::activeTextInput($model['dev_child'][$key], "[$key]child", ['class' => 'form-control', 'maxlength' => true]); ?>
                <?= Html::error($model['dev_child'][$key], "[$key]child", ['class' => 'help-block']); ?>
            <?= $form->field($model['dev_child'][$key], "[$key]child")->end(); ?>

        <?php endif; ?>

    <?php endforeach; ?>
    -->

    <template v-if="typeof dev.devChildren == 'object'">
        <template v-for="(value, key, index) in dev.devChildren">
            <template v-if="key == 'new'">
                <template v-for="(value, key, index) in dev.devChildren['new']">
                    <div v-bind:class="'form-group field-devchild-new-' + key + '-child'">
                        <label v-bind:for="'devchild-new-' + key + '-child'" class="control-label">Child</label>
                        <input v-bind:id="'devchild-new-' + key + '-child'" v-bind:name="'DevChild[new][' + key + '][child]'" class="form-control" type="text" v-model="dev.devChildren['new'][key].child">
                        <div class="help-block"></div>
                    </div>
                </template>
            </template>
            <template v-else>
                <div v-bind:class="'form-group field-devchild-' + key + '-child'">
                    <label v-bind:for="'devchild-' + key + '-child'" class="control-label">Child</label>
                    <input v-bind:id="'devchild-' + key + '-child'" v-bind:name="'DevChild[' + key + '][child]'" class="form-control" type="text" v-model="dev.devChildren[key].child">
                    <div class="help-block"></div>
                </div>
            </template>
        </template>
    </template>

    <a v-on:click="addChild" class="btn btn-default">Add Child</a>

    <hr class="margin-y-15">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton($model['dev']->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-default bg-azure rounded-xs border-azure']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default bg-lighter rounded-xs']); ?> 
        <?= Html::a('Back to list', ['index'], ['class' => 'btn btn-default bg-lightest rounded-xs pull-right']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>