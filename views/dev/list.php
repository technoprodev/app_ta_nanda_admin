<?php

use yii\helpers\Html;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/dev/list.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
?>

<table class="datatables display nowrap table table-striped table-hover table-condensed">
    <thead>
        <tr>
            <th class="text-dark f-normal" style="border-bottom: 1px">Action</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Id Combination</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Id User Defined</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Link</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Extend</th>
        </tr>
        <tr class="dt-search">
            <th class="padding-0"></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search id combination" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search id user defined" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search link" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search extend" class="form-control border-none f-normal padding-x-5"/></th>
        </tr>
    </thead>
</table>

<?php if ($this->context->can('create')): ?>
    <hr class="margin-y-15">
    <div>
        <?= Html::a('Create Data', ['create'], ['class' => 'btn btn-sm btn-default bg-azure rounded-xs border-azure']) ?>
    </div>
<?php endif; ?>