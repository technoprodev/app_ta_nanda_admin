<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Menu;
use common\yii\db\db;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Menu */
/* @var $form yii\widgets\ActiveForm */

// default value
if ($model->isNewRecord) {
    $model->enable = true;

    if (isset($parent))
        $model->parent = $parent;
}

?>

<div class='menu-form'>
    <?php ob_start(); ?>
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'url')->textInput() ?>
    <?= $form->field($model, 'parent')->dropDownList(Menu::option("title"), ['prompt' => '-- Pilih Data --']) ?>
    <?php $this->registerJs(
        '$(document).ready(function() { $("#menu-parent").select2(); });',
        yii\web\View::POS_END,
        null
    ) ?>

    <?= $form->field($model, 'icon')->textInput() ?>
    <?= $form->field($model, 'enable')->checkbox(['value' => true]) ?>
    <?= $form->field($model, '_permission')
        ->dropDownList(
            ArrayHelper::map(
                (new \yii\db\Query())->select("name")->from("auth_item")->where("type=2")->orderby("name")->all(),
                "name", "name"
            ),
            ['prompt' => '-- Pilih Data --']
        )
    ?>
    <?php $this->registerJs(
        '$(document).ready(function() { $("#menu-_permission").select2(); });',
        yii\web\View::POS_END,
        null
    ) ?>

    <hr>
    <div class="text-right">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <?= $this->render("@common/views/widget", [
        "attr" => ["class" => 'transparent ' . \Yii::$app->request->isAjax ? "col-md-12" : "col-md-8"],
        "title" => "Menu Information",
        "body" => ob_get_clean(),
    ]) ?>
    <div class="clearfix"></div>
</div>