<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$error = false;
$errorMessage = '';
if ($model['post']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['post'], ['class' => '']);
}

technosmart\assets_manager\SummernoteAsset::register($this);
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="row margin-left-30 m-margin-left-0">
    <div class="col-xs-12 col-md-6">    
<?php endif; ?>

<?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <?= $form->field($model['post'], 'title')->begin(); ?>
        <?= Html::activeLabel($model['post'], 'title', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['post'], 'title', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['post'], 'title', ['class' => 'help-block']); ?>
    <?= $form->field($model['post'], 'title')->end(); ?>

    <?= $form->field($model['post'], 'slug')->begin(); ?>
        <?= Html::activeLabel($model['post'], 'slug', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['post'], 'slug', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['post'], 'slug', ['class' => 'help-block']); ?>
    <?= $form->field($model['post'], 'slug')->end(); ?>

    <?= $form->field($model['post'], 'excerpt')->begin(); ?>
        <?= Html::activeLabel($model['post'], 'excerpt', ['class' => 'control-label']); ?>
        <?= Html::activeTextArea($model['post'], 'excerpt', ['class' => 'form-control', 'rows' => 6]) ?>
        <?= Html::error($model['post'], 'excerpt', ['class' => 'help-block']); ?>
    <?= $form->field($model['post'], 'excerpt')->end(); ?>

    <?= $form->field($model['post'], 'content')->begin(); ?>
        <?= Html::activeLabel($model['post'], 'content', ['class' => 'control-label']); ?>
        <?= Html::activeTextArea($model['post'], 'content', ['class' => 'form-control summernote-default', 'rows' => 6]) ?>
        <?= Html::error($model['post'], 'content', ['class' => 'help-block']); ?>
    <?= $form->field($model['post'], 'content')->end(); ?>

    <?= $form->field($model['post'], 'featured_image')->begin(); ?>
        <?= Html::activeLabel($model['post'], 'featured_image', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['post'], 'featured_image', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['post'], 'featured_image', ['class' => 'help-block']); ?>
    <?= $form->field($model['post'], 'featured_image')->end(); ?>

    <?= $form->field($model['post'], 'id_post_category', ['options' => ['class' => 'form-group radio-elegant']])->begin(); ?>
        <label for="post-id_post_category" class="control-label">Category</label>
        <?= Html::activeRadioList($model['post'], 'id_post_category', ArrayHelper::map(\technosmart\models\PostCategory::find()->indexBy('id')->asArray()->all(), 'id', 'name'), ['class' => 'row row-radio', 'unselect' => null,
            'item' => function($index, $label, $name, $checked, $value){
                $disabled = in_array($value, ['val1', 'val2']) ? 'disabled' : '';
                return "<div class='radio col-xs-3'><label><input type='radio' name='$name' value='$value' $disabled><i></i>$label</label></div>";
            }]); ?>
        <?= Html::error($model['post'], 'id_post_category', ['class' => 'help-block']); ?>
    <?= $form->field($model['post'], 'id_post_category')->end(); ?>


    <hr class="margin-y-15">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton($model['post']->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-default bg-azure rounded-xs border-azure']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default bg-lighter rounded-xs']); ?> 
        <?= Html::a('Back to list', ['index'], ['class' => 'btn btn-default bg-lightest rounded-xs pull-right']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>