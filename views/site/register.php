<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Register';
$this->params['breadcrumbs'][] = $this->title;

$error = false;
$errorMessage = '';
if ($model['user']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['user'], ['class' => '']);
}
?>

<div class="text-center margin-top-60 m-margin-top-30">
    <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/logo-inverse.png" width="50px;" class="bg-dark-azure shadow circle padding-5">
</div>
<div class="margin-top-20 border-lighter border-thick shadow rounded-xs" style="max-width: 350px; width: 100%; margin-left: auto; margin-right: auto;">
    <div class="clearfix padding-15 border-bottom bg-azure text-center fs-18">
        <span class="f-italic"><?= $this->title ?></span>
    </div>
    <div class="padding-20 bg-lightest">
        <?php $form = ActiveForm::begin(['id' => 'app']); ?>

            <?= $form->field($model['user'], 'email')->textInput(/*['autofocus' => true]*/) ?>

            <?= $form->field($model['user'], 'name')->textInput() ?>

            <?= $form->field($model['user'], 'password')->passwordInput() ?>

            <?php if ($error) : ?>
                <div class="alert alert-danger">
                    <?= $errorMessage ?>
                </div>
            <?php endif; ?>

            <div class="form-group">
                <?= Html::submitButton('Register', ['class' => 'btn btn-default bg-azure rounded-xs border-azure']) ?>
            </div>

        <?php ActiveForm::end(); ?>
    </div>
    <div class="bg-lighter padding-y-10">
        <div class="text-center text-azure fs-16"><?= Yii::$app->params['app.name'] ?></div>
        <div class="text-center fs-12 margin-top-2"><?= Yii::$app->params['app.author'] ?> © <?= date('Y') ?></div>
    </div>
</div>