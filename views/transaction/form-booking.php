<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$error = false;
$errorMessage = '';
if ($model['transaction']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['transaction'], ['class' => '']);
}
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="row margin-left-30 m-margin-left-0">
    <div class="col-xs-12 col-md-6">    
<?php endif; ?>

<?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <?= $form->field($model['transaction'], 'name')->begin(); ?>
        <?= Html::activeLabel($model['transaction'], 'name', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['transaction'], 'name', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['transaction'], 'name', ['class' => 'help-block']); ?>
    <?= $form->field($model['transaction'], 'name')->end(); ?>

    <?= $form->field($model['transaction'], 'phone')->begin(); ?>
        <?= Html::activeLabel($model['transaction'], 'phone', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['transaction'], 'phone', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['transaction'], 'phone', ['class' => 'help-block']); ?>
    <?= $form->field($model['transaction'], 'phone')->end(); ?>

    <?= $form->field($model['transaction'], 'email')->begin(); ?>
        <?= Html::activeLabel($model['transaction'], 'email', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['transaction'], 'email', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['transaction'], 'email', ['class' => 'help-block']); ?>
    <?= $form->field($model['transaction'], 'email')->end(); ?>

    <?= $form->field($model['transaction'], 'service_type')->begin(); ?>
        <?= Html::activeLabel($model['transaction'], 'service_type', ['class' => 'control-label']); ?>
        <?= Html::activeDropDownList($model['transaction'], 'service_type', [ 'klinik' => 'Klinik', 'petshop' => 'Petshop', 'grooming' => 'Grooming', ], ['prompt' => 'Choose one please', 'class' => 'form-control']) ?>
        <?= Html::error($model['transaction'], 'service_type', ['class' => 'help-block']); ?>
    <?= $form->field($model['transaction'], 'service_type')->end(); ?>

    <?= $form->field($model['transaction'], 'service_name')->begin(); ?>
        <?= Html::activeLabel($model['transaction'], 'service_name', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['transaction'], 'service_name', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['transaction'], 'service_name', ['class' => 'help-block']); ?>
    <?= $form->field($model['transaction'], 'service_name')->end(); ?>

    <?= $form->field($model['transaction'], 'harga')->begin(); ?>
        <?= Html::activeLabel($model['transaction'], 'harga', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['transaction'], 'harga', ['class' => 'form-control']) ?>
        <?= Html::error($model['transaction'], 'harga', ['class' => 'help-block']); ?>
    <?= $form->field($model['transaction'], 'harga')->end(); ?>
    
    <hr class="margin-y-15">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton('Simpan', ['class' => 'btn btn-default bg-azure rounded-xs border-azure']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default bg-lighter rounded-xs']); ?> 
        <?= Html::a('Back to Previous', ['create'], ['class' => 'btn btn-default bg-lightest rounded-xs pull-right']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>