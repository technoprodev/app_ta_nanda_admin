<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$error = false;
$errorMessage = '';
if ($model['transaction']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['transaction'], ['class' => '']);
}

$this->registerJsFile('@web/app/transaction/list-create-booking.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
$this->registerJsFile('@web/app/transaction/list-create-customer.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="row margin-left-30 m-margin-left-0">
    <div class="col-xs-12 col-md-12">    
<?php endif; ?>

<?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <div class="form-group radio-elegant">
        <label class="control-label">Jenis Transaksi</label>
        <div class="row row-radio">
            <div class="radio col-xs-3">
                <label>
                    <input name="Transaction[jenis]" value="1" type="radio" v-on:click="setJenisTransaksi(1)">
                    <i></i>Sudah pernah booking
                </label>
            </div>
            <div class="radio col-xs-3">
                <label>
                    <input name="Transaction[jenis]" value="2" type="radio" v-on:click="setJenisTransaksi(2)">
                    <i></i>Sudah pernah registrasi
                </label>
            </div>
            <div class="radio col-xs-3">
                <label>
                    <input name="Transaction[jenis]" value="3" type="radio" v-on:click="setJenisTransaksi(3)">
                    <i></i>Pelanggan baru
                </label>
            </div>
        </div>
    </div>

    <div v-show="transaction.jenis == 1">
        <table class="datatables-create-booking display nowrap table table-striped table-hover table-condensed">
            <thead>
                <tr>
                    <th class="text-dark f-normal" style="border-bottom: 1px">Action</th>
                    <th class="text-dark f-normal" style="border-bottom: 1px">Id Customer</th>
                    <th class="text-dark f-normal" style="border-bottom: 1px">Name</th>
                    <th class="text-dark f-normal" style="border-bottom: 1px">Phone</th>
                    <th class="text-dark f-normal" style="border-bottom: 1px">Email</th>
                    <th class="text-dark f-normal" style="border-bottom: 1px">Service Type</th>
                    <th class="text-dark f-normal" style="border-bottom: 1px">Service Name</th>
                    <th class="text-dark f-normal" style="border-bottom: 1px">Harga</th>
                    <th class="text-dark f-normal" style="border-bottom: 1px">Booking Date</th>
                    <th class="text-dark f-normal" style="border-bottom: 1px">Appoinment Date</th>
                    <th class="text-dark f-normal" style="border-bottom: 1px">Transaction Date</th>
                    <th class="text-dark f-normal" style="border-bottom: 1px">Note</th>
                </tr>
                <tr class="dt-search">
                    <th class="padding-0"></th>
                    <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search id customer" class="form-control border-none f-normal padding-x-5"/></th>
                    <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search name" class="form-control border-none f-normal padding-x-5"/></th>
                    <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search phone" class="form-control border-none f-normal padding-x-5"/></th>
                    <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search email" class="form-control border-none f-normal padding-x-5"/></th>
                    <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search service type" class="form-control border-none f-normal padding-x-5"/></th>
                    <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search service name" class="form-control border-none f-normal padding-x-5"/></th>
                    <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search harga" class="form-control border-none f-normal padding-x-5"/></th>
                    <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search booking date" class="form-control border-none f-normal padding-x-5"/></th>
                    <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search appoinment date" class="form-control border-none f-normal padding-x-5"/></th>
                    <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search transaction date" class="form-control border-none f-normal padding-x-5"/></th>
                    <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search note" class="form-control border-none f-normal padding-x-5"/></th>
                </tr>
            </thead>
        </table>
    </div>
    <div v-show="transaction.jenis == 2">
        <table class="datatables-create-customer display nowrap table table-striped table-hover table-condensed">
            <thead>
                <tr>
                    <th class="text-dark f-normal" style="border-bottom: 1px">Action</th>
                    <th class="text-dark f-normal" style="border-bottom: 1px">Email</th>
                    <th class="text-dark f-normal" style="border-bottom: 1px">Phone</th>
                    <th class="text-dark f-normal" style="border-bottom: 1px">Pet 1</th>
                    <th class="text-dark f-normal" style="border-bottom: 1px">Pet 2</th>
                    <th class="text-dark f-normal" style="border-bottom: 1px">Pet 3</th>
                    <th class="text-dark f-normal" style="border-bottom: 1px">Alamat</th>
                    <th class="text-dark f-normal" style="border-bottom: 1px">Birthplace</th>
                    <th class="text-dark f-normal" style="border-bottom: 1px">Birthday</th>
                    <th class="text-dark f-normal" style="border-bottom: 1px">Wife Name</th>
                    <th class="text-dark f-normal" style="border-bottom: 1px">Wife Email</th>
                </tr>
                <tr class="dt-search">
                    <th class="padding-0"></th>
                    <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search email" class="form-control border-none f-normal padding-x-5"/></th>
                    <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search phone" class="form-control border-none f-normal padding-x-5"/></th>
                    <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search pet 1" class="form-control border-none f-normal padding-x-5"/></th>
                    <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search pet 2" class="form-control border-none f-normal padding-x-5"/></th>
                    <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search pet 3" class="form-control border-none f-normal padding-x-5"/></th>
                    <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search alamat" class="form-control border-none f-normal padding-x-5"/></th>
                    <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search birthplace" class="form-control border-none f-normal padding-x-5"/></th>
                    <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search birthday" class="form-control border-none f-normal padding-x-5"/></th>
                    <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search wife name" class="form-control border-none f-normal padding-x-5"/></th>
                    <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search wife email" class="form-control border-none f-normal padding-x-5"/></th>
                </tr>
            </thead>
        </table>
    </div>
    <div v-show="transaction.jenis == 3">
        <?= $form->field($model['transaction'], 'name')->begin(); ?>
            <?= Html::activeLabel($model['transaction'], 'name', ['class' => 'control-label']); ?>
            <?= Html::activeTextInput($model['transaction'], 'name', ['class' => 'form-control', 'maxlength' => true]) ?>
            <?= Html::error($model['transaction'], 'name', ['class' => 'help-block']); ?>
        <?= $form->field($model['transaction'], 'name')->end(); ?>

        <?= $form->field($model['transaction'], 'phone')->begin(); ?>
            <?= Html::activeLabel($model['transaction'], 'phone', ['class' => 'control-label']); ?>
            <?= Html::activeTextInput($model['transaction'], 'phone', ['class' => 'form-control', 'maxlength' => true]) ?>
            <?= Html::error($model['transaction'], 'phone', ['class' => 'help-block']); ?>
        <?= $form->field($model['transaction'], 'phone')->end(); ?>

        <?= $form->field($model['transaction'], 'email')->begin(); ?>
            <?= Html::activeLabel($model['transaction'], 'email', ['class' => 'control-label']); ?>
            <?= Html::activeTextInput($model['transaction'], 'email', ['class' => 'form-control', 'maxlength' => true]) ?>
            <?= Html::error($model['transaction'], 'email', ['class' => 'help-block']); ?>
        <?= $form->field($model['transaction'], 'email')->end(); ?>

        <?= $form->field($model['transaction'], 'service_type')->begin(); ?>
            <?= Html::activeLabel($model['transaction'], 'service_type', ['class' => 'control-label']); ?>
            <?= Html::activeDropDownList($model['transaction'], 'service_type', [ 'klinik' => 'Klinik', 'petshop' => 'Petshop', 'grooming' => 'Grooming', ], ['prompt' => 'Choose one please', 'class' => 'form-control']) ?>
            <?= Html::error($model['transaction'], 'service_type', ['class' => 'help-block']); ?>
        <?= $form->field($model['transaction'], 'service_type')->end(); ?>

        <?= $form->field($model['transaction'], 'service_name')->begin(); ?>
            <?= Html::activeLabel($model['transaction'], 'service_name', ['class' => 'control-label']); ?>
            <?= Html::activeTextInput($model['transaction'], 'service_name', ['class' => 'form-control', 'maxlength' => true]) ?>
            <?= Html::error($model['transaction'], 'service_name', ['class' => 'help-block']); ?>
        <?= $form->field($model['transaction'], 'service_name')->end(); ?>

        <?= $form->field($model['transaction'], 'harga')->begin(); ?>
            <?= Html::activeLabel($model['transaction'], 'harga', ['class' => 'control-label']); ?>
            <?= Html::activeTextInput($model['transaction'], 'harga', ['class' => 'form-control']) ?>
            <?= Html::error($model['transaction'], 'harga', ['class' => 'help-block']); ?>
        <?= $form->field($model['transaction'], 'harga')->end(); ?>
        
        <hr class="margin-y-15">

        <?php if ($error) : ?>
            <div class="alert alert-danger">
                <?= $errorMessage ?>
            </div>
        <?php endif; ?>
        
        <div class="form-group clearfix">
            <?= Html::submitButton('Simpan', ['class' => 'btn btn-default bg-azure rounded-xs border-azure']) ?>
            <?= Html::resetButton('Reset', ['class' => 'btn btn-default bg-lighter rounded-xs']); ?> 
            <?= Html::a('Back to list', ['index'], ['class' => 'btn btn-default bg-lightest rounded-xs pull-right']) ?>
        </div>
    </div>
    
<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>