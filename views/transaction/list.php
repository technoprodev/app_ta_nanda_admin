<?php

use yii\helpers\Html;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/transaction/list.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
?>

<table class="datatables display nowrap table table-striped table-hover table-condensed">
    <thead>
        <tr>
            <th class="text-dark f-normal" style="border-bottom: 1px">Action</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Id Customer</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Name</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Phone</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Email</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Service Type</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Service Name</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Harga</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Booking Date</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Appoinment Date</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Transaction Date</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Note</th>
        </tr>
        <tr class="dt-search">
            <th class="padding-0"></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search id customer" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search name" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search phone" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search email" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search service type" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search service name" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search harga" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search booking date" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search appoinment date" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search transaction date" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search note" class="form-control border-none f-normal padding-x-5"/></th>
        </tr>
    </thead>
</table>

<?php if ($this->context->can('create')): ?>
    <hr class="margin-y-15">
    <div>
        <?= Html::a('Create Transaction', ['create'], ['class' => 'btn btn-sm btn-default bg-azure rounded-xs border-azure']) ?>
    </div>
<?php endif; ?>