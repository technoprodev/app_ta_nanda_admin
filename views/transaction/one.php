<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

if (isset($model['transaction']->customer->name)) {
    $name = $model['transaction']->customer->name;
} else if (isset($model['transaction']->name)) {
    $name = $model['transaction']->name;
} else {
    $name = '<span class="text-gray f-italic">(kosong)</span>';
}
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="row">
    <div class="col-xs-8 margin-top-15">
<?php endif; ?>

<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 m-padding-x-0 text-right m-text-left">Nama Customer</div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $name ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 m-padding-x-0 text-right m-text-left"><?= $model['transaction']->attributeLabels()['service_type'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['transaction']->service_type ? $model['transaction']->service_type : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 m-padding-x-0 text-right m-text-left"><?= $model['transaction']->attributeLabels()['service_name'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['transaction']->service_name ? $model['transaction']->service_name : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 m-padding-x-0 text-right m-text-left"><?= $model['transaction']->attributeLabels()['harga'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['transaction']->harga ? $model['transaction']->harga : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 m-padding-x-0 text-right m-text-left"><?= $model['transaction']->attributeLabels()['transaction_date'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['transaction']->transaction_date ? $model['transaction']->transaction_date : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 m-padding-x-0 text-right m-text-left"><?= $model['transaction']->attributeLabels()['note'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['transaction']->note ? $model['transaction']->note : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<?php if (!Yii::$app->request->isAjax) : ?>
        <hr class="margin-y-15">
        <div class="form-group clearfix">
            <?= Html::a('Tambah Transaksi', ['create'], ['class' => 'btn btn-sm btn-default bg-azure rounded-xs border-azure']) ?>&nbsp;
            <?= Html::a('Print', ['index'], ['class' => 'btn btn-sm btn-default bg-lighter rounded-xs pull-right']) ?>
        </div>
    </div>
</div>
<?php endif; ?>