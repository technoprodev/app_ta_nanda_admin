<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

technosmart\assets_manager\Select2Asset::register($this);

$error = false;
$errorMessage = '';
if ($model['userIdentity']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['userIdentity'], ['class' => '']);
}
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="row margin-left-30 m-margin-left-0">
    <div class="col-xs-12 col-md-6">    
<?php endif; ?>
    
<?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <div class="box box-break-sm">
        <div class="box-12 padding-x-0">
            <?= $form->field($model['userIdentity'], 'name')->begin(); ?>
                <?= Html::activeLabel($model['userIdentity'], 'name', ['class' => 'control-label']); ?>
                <?= Html::activeTextInput($model['userIdentity'], 'name', ['class' => 'form-control', 'maxlength' => true]); ?>
                <?= Html::error($model['userIdentity'], 'name', ['class' => 'help-block']); ?>
            <?= $form->field($model['userIdentity'], 'name')->end(); ?>
        </div>
    </div>

    <div class="box box-break-sm">
        <div class="box-6 padding-x-0">
            <?= $form->field($model['userIdentity'], 'username')->begin(); ?>
                <?= Html::activeLabel($model['userIdentity'], 'username', ['class' => 'control-label']); ?>
                <?= Html::activeTextInput($model['userIdentity'], 'username', ['class' => 'form-control', 'maxlength' => true]); ?>
                <?= Html::error($model['userIdentity'], 'username', ['class' => 'help-block']); ?>
            <?= $form->field($model['userIdentity'], 'username')->end(); ?>
        </div>
        <div class="box-6 padding-right-0">
            <?= $form->field($model['userIdentity'], 'email')->begin(); ?>
                <?= Html::activeLabel($model['userIdentity'], 'email', ['class' => 'control-label']); ?>
                <?= Html::activeTextInput($model['userIdentity'], 'email', ['class' => 'form-control', 'maxlength' => true]); ?>
                <?= Html::error($model['userIdentity'], 'email', ['class' => 'help-block']); ?>
            <?= $form->field($model['userIdentity'], 'email')->end(); ?>
        </div>
    </div>

    <?php if($model['userIdentity']->isNewRecord) : ?>

        <div class="box box-break-sm">
            <div class="box-6 padding-x-0">
                <?= $form->field($model['userIdentity'], 'password')->begin(); ?>
                    <?= Html::activeLabel($model['userIdentity'], 'password', ['class' => 'control-label']); ?>
                    <?= Html::activePasswordInput($model['userIdentity'], 'password', ['class' => 'form-control', 'maxlength' => true]); ?>
                    <?= Html::error($model['userIdentity'], 'password', ['class' => 'help-block']); ?>
                <?= $form->field($model['userIdentity'], 'password')->end(); ?>
            </div>
            <div class="box-6 padding-right-0">
                <?= $form->field($model['userIdentity'], 'repassword')->begin(); ?>
                    <?= Html::activeLabel($model['userIdentity'], 'repassword', ['class' => 'control-label']); ?>
                    <?= Html::activePasswordInput($model['userIdentity'], 'repassword', ['class' => 'form-control', 'maxlength' => true]); ?>
                    <?= Html::error($model['userIdentity'], 'repassword', ['class' => 'help-block']); ?>
                <?= $form->field($model['userIdentity'], 'repassword')->end(); ?>
            </div>
        </div>

    <?php endif; ?>

    <?php if(!$model['userIdentity']->isNewRecord) : ?>
        
        <div class="box box-break-sm">
            <div class="box-6 padding-x-0">
                <?= $form->field($model['userIdentity'], 'status', ['options' => ['class' => 'form-group radio-elegant']])->begin(); ?>
                    <?= Html::activeLabel($model['userIdentity'], 'status', ['class' => 'control-label']); ?>
                    <?= Html::activeRadioList($model['userIdentity'], 'status', ['1' => 'Active', '0' => 'Inactive', '-1' => 'Deleted'], ['unselect' => null,
                        'item' => function($index, $label, $name, $checked, $value){
                            $disabled = in_array($value, []) ? true : false;

                            $radio = Html::radio($name, $checked, ['value' => $value, 'disabled' => $disabled]);
                            return Html::tag('div', Html::label($radio . '<i></i>' . $label), ['class' => 'radio']);

                            // equal to this
                            // $disabled = in_array($value, ['val1', 'val2', '1']) ? 'disabled' : '';
                            // return "<div class='radio'><label><input type='radio' name='$name' value='$value' $disabled><i></i>My Label</label></div>";
                        }]); ?>
                    <?= Html::error($model['userIdentity'], 'status', ['class' => 'help-block']); ?>
                <?= $form->field($model['userIdentity'], 'status')->end(); ?>
            </div>
        </div>

    <?php endif; ?>

    <div class="box box-break-sm">
        <div class="box-12 padding-x-0">
            <div class="form-group field-roles-assignments">
                <label class="control-label">Roles</label>
                <?= Html::checkboxList(
                    'assignments[]',
                    $model['assignments'],
                    $roles
                ) ?>
            </div>
        </div>
    </div>

    <hr class="margin-y-15">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton($model['userIdentity']->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-default bg-azure rounded-xs border-azure']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default bg-lighter rounded-xs']); ?> 
        <?= Html::a('Back to list', ['index'], ['class' => 'btn btn-default bg-lightest rounded-xs pull-right']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>