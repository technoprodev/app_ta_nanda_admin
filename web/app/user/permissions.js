$(document).ready(function() {
    $("input[type='checkbox'].check-all").change(function() {
    	var index = $(this).parents("th").index();
    	var checked = $(this).is(":checked");

    	$(this).parents("table").find("tbody td").each(function() {
    		if ($(this).index() == index)
    			$("input[type='checkbox']", this).prop("checked", checked);
    	})
    })
});